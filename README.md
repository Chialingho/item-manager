# ItemManager

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.0.

There are the filter criterias at the first page, after selecting the element, please click Apply Filter for getting the results. and click Clear Filter to remove the filtering and go back to original list of items.

Sorting buttons are indicated with ↓↑. When sorting action applied, the according button will be green. Sorting can not reverse back. 

You are able to add items into Favorite list by clicking Fav button under each item and to remove it from the list, you are able to do that at the item on the page or at the item on the modal.

To see Favortie item list, please click on "Show Favortie List" and a modal will pop up. You are able to search the item in favorite list by their title name and able to remove it by clicking on the delete button. 
Modal is able to be closed by an X button placed at the right corner of the modal or by clikcing outside the modal. 


## Instruction

First clone this repo, add this line in the terminal: `git clone https://Chialingho@bitbucket.org/Chialingho/item-manager.git` to your local folder.
After cloning, you shall see all of the files. 

Second, go to `/item-manager` do `npm i` at the terminal to install node_modules.

Lastly, to run in your local host, please go to folder '/item-manager' and then run `ng serve --open`. This will automatically generate a window in your browser at the link `http://localhost:4200/`



