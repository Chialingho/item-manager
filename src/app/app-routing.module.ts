import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { ShowItemComponent } from './components/show-item/show-item.component'
import { MainComponent } from './page/main/main.component'
import { ItemPageComponent } from './page/item-page/item-page.component'

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      { path: '', redirectTo: 'item-page', pathMatch: 'full' },
      { path: 'item-page', component: ItemPageComponent }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
