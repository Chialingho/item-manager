import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { ShowItemComponent } from './components/show-item/show-item.component'
import { MainComponent } from './page/main/main.component'

import { HttpClientModule } from '@angular/common/http'
import { FilterItemsComponent } from './components/filter-items/filter-items.component'
import { NavComponent } from './components/nav/nav.component'
import { ItemPageComponent } from './page/item-page/item-page.component'
import { ModalComponent } from './components/modal/modal.component'
import { SortComponent } from './components/sort/sort.component'
import { NoFoundComponent } from './components/no-found/no-found.component'

@NgModule({
  declarations: [
    AppComponent,
    ShowItemComponent,
    MainComponent,

    FilterItemsComponent,

    NavComponent,

    ItemPageComponent,

    ModalComponent,

    SortComponent,

    NoFoundComponent
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
