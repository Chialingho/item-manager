import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { Item } from 'src/app/interfaces/item'
import { ApiServiceService } from 'src/app/services/api-service.service'
import { first } from 'rxjs/operators'

@Component({
  selector: 'app-filter-items',
  templateUrl: './filter-items.component.html',
  styleUrls: ['./filter-items.component.css'],
  host: {
    '(document:click)': 'onClick($event)'
  }
})
export class FilterItemsComponent implements OnInit {
  @Input() priceBegin: number
  @Input() priceEnd: number
  @Input() keyWords: string
  @Input() items: Item[]
  @Input() componentId
  @Output() itemSelectedValueEvent: EventEmitter<Object> = new EventEmitter<
    string
  >()
  @Output() itemPriceEvent: EventEmitter<Object> = new EventEmitter<string>()
  @Input() clearInfo
  @Input() clearTitleEmail

  menu: boolean = false
  inputElement: boolean
  rangeElement: boolean
  descriptionElement: boolean
  value: string = ''
  firstMatchElement: string
  filterApplied: boolean = false

  constructor (private apiService: ApiServiceService) {}

  ngOnInit () {
    if (this.componentId === 'title' || this.componentId === 'email') {
      this.inputElement = true
    }

    if (this.componentId === 'price') {
      this.rangeElement = true
    }

    if (this.componentId === 'description') {
      this.descriptionElement = true
    }
  }

  ngDoCheck () {
    if (this.clearInfo) {
      this.priceBegin = null
      this.priceEnd = null
      this.keyWords = ''
    }
    if (this.clearTitleEmail) {
      this.value = ''
    }
  }

  onClick (event): void {
    if (event.target.id !== this.componentId) {
      this.menu = false
    } else {
      this.menu = !this.menu
    }
    if (this.menu) {
      this.value = ''
    }
  }

  handleOnChange (value) {
    this.menu = true
    let ItemsProperty = this.ArrayOfExtractValues()
    if (value === '') {
      return ['']
    }
    let firstMatch = this.filterFunction(ItemsProperty, value).slice(0, 1)
    this.adjustScrollPosition('listId', firstMatch[0])
    this.firstMatchElement = firstMatch[0]
    return firstMatch
  }

  handlePrice (priceBegin: number, priceEnd: number): void {
    setTimeout(() => {
      if (priceBegin === undefined) {
        priceBegin = 0
      }
      if (priceEnd === undefined) {
        priceEnd = 0
      }
      let info = {}
      info[this.componentId] = [priceBegin, priceEnd]
      this.itemPriceEvent.emit(info)
    }, 1000)
  }

  handleKeyWords (keywords: string): void {
    setTimeout(() => {
      let info = {}
      info[this.componentId] = keywords
      this.itemSelectedValueEvent.emit(info)
    }, 1000)
  }

  adjustScrollPosition (listId, itemId): void {
    const list = document.getElementById(listId)
    const item = document.getElementById(itemId)
    if (item) {
      if (list.scrollHeight > list.clientHeight) {
        const scrollBottom = list.clientHeight + list.scrollTop
        const elementBottom = item.offsetTop + item.offsetHeight
        if (elementBottom > scrollBottom) {
          list.scrollTop = elementBottom - list.clientHeight
        } else if (item.offsetTop < list.scrollTop) {
          list.scrollTop = item.offsetTop
        }
      }
    } else {
      list.scrollTop = 0
    }
  }

  ArrayOfExtractValues (): Item[] {
    let newItems: any = this.items
    newItems = this.items.map(item => {
      return item[this.componentId]
    })
    return newItems
  }

  filterFunction (options = [], filter): any {
    return options.filter(option => {
      const matches = option.toLowerCase().indexOf(filter.toLowerCase()) === 0
      return matches
    })
  }

  filteredItemStyle (item, value): string {
    if (item === this.handleOnChange(value)[0]) {
      return 'lightgray'
    } else return ''
  }

  selectedItem (item, id): void {
    let info = {}
    info[id] = item[id]
    this.value = item[id]

    this.itemSelectedValueEvent.emit(info)

    this.menu = false
  }

  handleEnter (): void {
    let info = {}
    info[this.componentId] = this.firstMatchElement
    this.itemSelectedValueEvent.emit(info)
    this.value = this.firstMatchElement
    this.menu = false
  }
}
