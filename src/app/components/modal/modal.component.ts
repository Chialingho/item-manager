import { Component, OnInit, Output, EventEmitter } from '@angular/core'
import { FavoriteService } from 'src/app/services/favorite.service'
import { Item } from 'src/app/interfaces/item'

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  @Output() closeModalEvent: EventEmitter<boolean> = new EventEmitter<boolean>()
  favList: Item[] = this.favService.showList()
  title: string
  noSearchMatch: boolean = false
  noFavItem: boolean = false

  constructor (private favService: FavoriteService) {}

  ngOnInit () {}

  closeModal (): void {
    this.closeModalEvent.emit(false)
  }

  removeItem (item): void {
    this.noFavItem = false
    this.favService.deleteFav(item)
    this.favList = this.favService.showList()
    if (this.favList.length === 0) {
      this.noFavItem = true
    }
  }

  handleSearchTitle (title): void {
    this.noSearchMatch = false
    this.favList = this.favService.searchTitle(title)
    if (this.favList.length === 0) {
      this.noSearchMatch = true
    }
  }

  handleEnter () {
    this.title = this.favList[0].title
  }
}
