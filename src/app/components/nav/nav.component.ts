import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core'
import { Item } from 'src/app/interfaces/item'
import { ApiServiceService } from 'src/app/services/api-service.service'

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  @Output() itemSelectedValueEvent: EventEmitter<Object> = new EventEmitter<
    string
  >()
  @Input() clearInfo
  @Input() clearTitleEmail

  items: Item[] = []
  selectedItemInfo: Object

  constructor (private apiService: ApiServiceService) {}

  ngOnInit () {
    this.apiService.fetchData().subscribe(items => {
      this.items = items['items']
    })
  }

  receiveItem ($event) {
    this.selectedItemInfo = $event
    this.itemSelectedValueEvent.emit(this.selectedItemInfo)
  }
}
