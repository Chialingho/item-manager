import { Component, OnInit, Input } from '@angular/core'

@Component({
  selector: 'app-no-found',
  templateUrl: './no-found.component.html',
  styleUrls: ['./no-found.component.css']
})
export class NoFoundComponent implements OnInit {
  @Input() noSearchMatch: boolean
  @Input() noFavItem: boolean
  @Input() pageNoMatch: boolean

  constructor () {}

  ngOnInit () {}
}
