import { Component, OnInit, Input, SimpleChanges } from '@angular/core'
import { Item } from 'src/app/interfaces/item'
import { FavoriteService } from 'src/app/services/favorite.service'

@Component({
  selector: 'app-show-item',
  templateUrl: './show-item.component.html',
  styleUrls: ['./show-item.component.css']
})
export class ShowItemComponent implements OnInit {
  @Input() items: Item[]
  @Input() jumpToFirstPage: boolean

  itemsCopy: Item[] = []
  disableAddFav: boolean = false
  disableDelete: boolean = false
  prev: number = 0
  next: number = 5

  constructor (private favService: FavoriteService) {}

  newItems (): Item[] {
    this.itemsCopy = this.items.slice(this.prev, this.next)
    return this.itemsCopy
  }

  ngOnInit () {}

  ngDoCheck () {
    if (this.jumpToFirstPage) {
      this.prev = 0
      this.next = 5
    }
  }

  toFirstPage (): void {
    if (this.jumpToFirstPage) {
      this.prev = 0
      this.next = 5
    }
  }

  forwardPage (): void {
    this.next < this.items.length && ((this.next += 5), (this.prev += 5))
  }

  backwardPage (): void {
    this.prev > 0 && ((this.next -= 5), (this.prev -= 5))
  }

  addFav (item): void {
    this.favService.addFav(item)
    this.disableAddFav = true
    this.disableDelete = false
  }

  deleteFav (item): void {
    this.favService.deleteFav(item)
    this.disableDelete = true
    this.disableAddFav = false
  }

  disabledOrNot (item): boolean {
    let favList = this.favService.showList()
    if (favList.includes(item)) {
      return true
    }
  }
}
