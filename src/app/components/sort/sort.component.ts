import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'

@Component({
  selector: 'app-sort',
  templateUrl: './sort.component.html',
  styleUrls: ['./sort.component.css']
})
export class SortComponent implements OnInit {
  @Input() title
  @Input() email
  @Input() price
  @Input() description
  @Output() criteraToSortEvent: EventEmitter<string> = new EventEmitter<
    string
  >()

  criteriaSlected: string

  constructor () {}

  ngOnInit () {}

  sendInfo (criteria): void {
    this.criteraToSortEvent.emit(criteria)
    this.criteriaSlected = criteria
  }
}
