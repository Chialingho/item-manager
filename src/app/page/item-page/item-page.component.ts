import { Component, OnInit } from '@angular/core'
import { Item } from 'src/app/interfaces/item'
import { ApiServiceService } from 'src/app/services/api-service.service'

@Component({
  selector: 'app-item-page',
  templateUrl: './item-page.component.html',
  styleUrls: ['./item-page.component.css'],
  host: {
    '(document:click)': 'onClick($event)'
  }
})
export class ItemPageComponent implements OnInit {
  showModal: boolean = false
  items: Item[] = []
  copyItems: Item[]
  receivedItemInfo: Item
  jumpToFirstPage: boolean = false
  stored_criteria: string
  clearInfo: boolean = false
  clearTitleEmail: boolean = false
  results: string

  constructor (private apiService: ApiServiceService) {}

  ngOnInit () {
    this.fetchData()
  }

  onClick (event): void {
    if (
      this.showModal &&
      event.target.id !== 'modal' &&
      event.target.id !== 'openModal'
    ) {
      this.showModal = false
    }
  }

  receiveItem ($event): void {
    this.receivedItemInfo = { ...this.receivedItemInfo, ...$event }
  }

  receiveCloseModalEvent ($event): void {
    this.showModal = $event
  }

  fetchData (): void {
    this.apiService.fetchData().subscribe(fetchItems => {
      this.items = fetchItems['items']
      this.copyItems = this.items
    })
  }

  filterApplied () {
    this.items = this.copyItems
    this.filter(this.receivedItemInfo)
    this.jumpToFirstPage = true
    this.clearInfo = true
    setTimeout(() => {
      this.jumpToFirstPage = false
      this.clearInfo = false
    }, 10)
  }

  clearAllFilter () {
    this.receivedItemInfo = undefined
    this.clearTitleEmail = true
    this.items = this.copyItems
    this.jumpToFirstPage = true
    setTimeout(() => {
      this.jumpToFirstPage = false
      this.clearTitleEmail = false
    }, 10)
  }

  filter (receivedItem: Object): void {
    if (receivedItem === undefined) {
      return
    }
    if (receivedItem.hasOwnProperty('price')) {
      this.items = this.items.filter(item => {
        return (
          item['price'] >= receivedItem['price'][0] &&
          item['price'] <= receivedItem['price'][1]
        )
      })
    }
    if (receivedItem.hasOwnProperty('description')) {
      this.items = this.items.filter(item => {
        return (
          item['description']
            .toLowerCase()
            .indexOf(receivedItem['description'].toLowerCase()) === 0
        )
      })
    }
    delete receivedItem['description']
    delete receivedItem['price']
    this.items = this.items.filter(item => {
      for (let key in receivedItem) {
        if (item[key] === undefined || item[key] != receivedItem[key]) {
          return false
        }
      }
      return true
    })
  }

  openModal (): void {
    this.showModal = true
  }

  receiveCriterta ($event): void {
    this.items = this.items.sort((a, b) => {
      if ($event === 'price') {
        return a[$event] - b[$event]
      } else if (a[$event] < b[$event]) {
        return -1
      } else if (a[$event] === b[$event]) {
        return 0
      } else return 1
    })
  }
}
