import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { catchError } from 'rxjs/operators'
import { base_url } from '../../config/config'

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  constructor (private http: HttpClient) {}
  fetchData (): Observable<Object> {
    return this.http.get(base_url, this.httpOptions).pipe(
      catchError(err => {
        console.log('error', err)
        return of([])
      })
    )
  }
}
