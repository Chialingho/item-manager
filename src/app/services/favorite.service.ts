import { Injectable } from '@angular/core'
import { Item } from '../interfaces/item'

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {
  favList: Item[] = []

  constructor () {}

  addFav (item: Item): void {
    this.favList = this.favList.concat(item)
  }

  deleteFav (toDeleteitem: Item): void {
    this.favList = this.favList.filter(item => {
      return item !== toDeleteitem
    })
  }

  showList (): Item[] {
    return this.favList
  }

  searchTitle (title): Item[] {
    return this.favList.filter(option => {
      return option.title.toLowerCase().indexOf(title.toLowerCase()) === 0
    })
  }
}
